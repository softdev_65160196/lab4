/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }

    public void showWelcome() {
        System.out.println("Welcome to OX");
    }

    public void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    public void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row,col: ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if(table.setRowCol(row, col)) {
                break;
            }
        }
    }

    public void newGame() {
        this.table = new Table(player1, player2);
    }

    public void play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                showInfo();
                if(playAgain()) {
                    newGame();
                } else{
                    break;
                }
            }
            if (table.checkDraw()) {
                showTable();
                showInfo();
                if(playAgain()) {
                    newGame();
                } else{
                    break;
                }
            }
            table.switchPlayer();
        }
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    public boolean playAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Wanna play again?(Y/N): ");
        String playAgain = sc.next();
        if(playAgain.equals("Y")) {
            return true;
        } else if(playAgain.equals("N")){
            return false;
        }
        return false;
    }
}    
